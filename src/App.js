import logo from './logo.svg';
import './App.css';
import { Navbar, Container, Nav, NavDropdown } from 'react-bootstrap';
import { DropdownSubmenu, NavDropdownMenu} from "react-bootstrap-submenu";

function App() {
  return (
    <div className="App">
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="#home">Home</Nav.Link>
              <NavDropdownMenu href="#players-handbook" title="Player's Handbook" id="collasible-nav-dropdown">
                <NavDropdown.Item href="#players-handbook">Index</NavDropdown.Item>
                <DropdownSubmenu href="#players-handbook/character-creation-and-leveling" title="Chapter 1 Character Creation and Leveling">
                  <NavDropdown.Item href="#players-handbook/character-creation-and-leveling">Index</NavDropdown.Item>
                  <NavDropdown.Item href="#players-handbook/character-creation-and-leveling/character-creation">Character Creation</NavDropdown.Item>
                  <NavDropdown.Item href="#players-handbook/character-creation-and-leveling/experience-points">Experience Points</NavDropdown.Item>
                  <NavDropdown.Item href="#players-handbook/character-creation-and-leveling/derived-stats">Derived Stats</NavDropdown.Item>
                </DropdownSubmenu>
                <DropdownSubmenu href="#players-handbook/classes" title="Chapter 2 Classes">
                  <NavDropdown.Item href="#players-handbook/classes">Index</NavDropdown.Item>
                  <DropdownSubmenu href="#players-handbook/classes/basic-classes" title="Basic Classes">
                    <NavDropdown.Item href="#players-handbook/classes/basic-classes">Index</NavDropdown.Item>
                    <NavDropdown.Item href="#players-handbook/classes/basic-classes/grand-devourer">Grand Devourer</NavDropdown.Item>
                    <NavDropdown.Item href="#players-handbook/classes/basic-classes/skiffmaster">Skiffmaster</NavDropdown.Item>
                    <NavDropdown.Item href="#players-handbook/classes/basic-classes/duelist">Duelist</NavDropdown.Item>
                    <NavDropdown.Item href="#players-handbook/classes/basic-classes/vessel">Vessel</NavDropdown.Item>
                    <NavDropdown.Item href="#players-handbook/classes/basic-classes/techie">Techie</NavDropdown.Item>
                    <NavDropdown.Item href="#players-handbook/classes/basic-classes/ghost-hunter">Ghost Hunter</NavDropdown.Item>
                  </DropdownSubmenu>
                </DropdownSubmenu>
              </NavDropdownMenu>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
